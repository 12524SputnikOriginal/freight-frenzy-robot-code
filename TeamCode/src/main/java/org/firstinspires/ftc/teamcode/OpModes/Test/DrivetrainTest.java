package org.firstinspires.ftc.teamcode.OpModes.Test;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
import org.firstinspires.ftc.teamcode.Modules.Intake.Intake;

@TeleOp(name = "Drivetest")
public class DrivetrainTest extends LinearOpMode {
    Drivetrain drivetrain;
    Intake intake;

    @Override
    public void runOpMode() {
        drivetrain = new Drivetrain(this);
        intake = new Intake(this);

        waitForStart();
        while (opModeIsActive()) {
            if (gamepad2.y)


            drivetrain.drive(gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_trigger - gamepad1.left_trigger);
        }
    }
}


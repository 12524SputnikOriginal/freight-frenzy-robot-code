package org.firstinspires.ftc.teamcode.OpModes.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Carousel;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
@Autonomous (name = "Blue")
@Disabled
public class BlueMoscow extends LinearOpMode {
    private Drivetrain dt;
    private Carousel cl;

    @Override
    public void runOpMode() {
        dt = new Drivetrain(this);
        cl = new Carousel(this);

        waitForStart();
        /*dt.backward(200);
        sleep(400);

        cl.motion(0.5f);
        sleep(2500);
        dt.forward(400);
        cl.motion(0);
        dt.rightward(700);
        sleep(500);
        dt.forward(6000);
        dt.stop();


        dt.forward(10);
        dt.angle90(true, 4000);
        dt.backward(200);
        dt.angle90(true, 4000);
        dt.backward(50);
        dt.stop();

        // тут карусель
        // cl.start();
        // sleep(100);
        // cl.stop();

        dt.forward(10);
        dt.leftward(20);
        dt.angle90(false);
        dt.leftward(50);
        dt.forward(200);
        dt.stop();
        */
    }
}

package org.firstinspires.ftc.teamcode.OpModes.Test;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
import org.firstinspires.ftc.teamcode.Modules.ImuSensor;

@Autonomous(name = "InfiniteRotation")
public class InfiniteRotation extends LinearOpMode {
    @Override
    public void runOpMode(){
        Drivetrain dt = new Drivetrain(this);
        ImuSensor imu = new ImuSensor(this);
        waitForStart();
        while (opModeIsActive()){

            telemetry.addData("Angle", imu.getDegrees());
            telemetry.update();
            dt.rotate(90);
            sleep(1000);
            dt.rotate(-90);
            sleep(1000);
            dt.rotate(180);
            sleep(1000);
            dt.rotate(-180);
            sleep(1000);
            dt.rotate(-90);
            sleep(1000);
            sleep(1000);
            sleep(1000);
            //dt.rotate(45);
            //sleep(2000);
            //dt.rotate(-180);
            //sleep(3000);
            //dt.setCourse(90);
            //dt.setCourse(180);
            //dt.setCourse(0);
            //dt.driveSteps(0, 0, 0.5, 20000);
            stop();
        }
        stop();
    }
}

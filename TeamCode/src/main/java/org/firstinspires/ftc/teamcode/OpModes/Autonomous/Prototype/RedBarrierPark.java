package org.firstinspires.ftc.teamcode.OpModes.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;

@Autonomous(name = "RedBarrierPark")
@Disabled
public class RedBarrierPark extends LinearOpMode{
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);

        waitForStart();

        dt.driveSteps(0, -1, 0, 1200);
        //dt.driveSteps(-1, 0, 0, 1750);
        dt.stop();
    }
}
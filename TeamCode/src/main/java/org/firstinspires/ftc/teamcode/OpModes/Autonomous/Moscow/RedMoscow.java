package org.firstinspires.ftc.teamcode.OpModes.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Carousel;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;

@Autonomous (name = "Red")
@Disabled
public class RedMoscow extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Carousel cl = new Carousel(this);

        waitForStart();

        /*dt.backward(1000);
        sleep(500);
        dt.forward(800);
        sleep(500);
        dt.angle90(false);
        sleep(400);
        dt.backward(2300);
        sleep(300);
        dt.angle120(false);
        sleep(800);
        dt.backward(200);
        dt.stop();

        // тут карусель
        cl.motion(.5);
        sleep(1000);
        cl.motion(0);


        dt.forward(100);
        sleep(300);
        dt.rightward(200);
        sleep(300);
        dt.angle90(true);
        sleep(300);
        dt.rightward(100);
        sleep(300);
        dt.forward(5000);
        sleep(300);
        dt.stop();

        dt.forward(5000);
        sleep(300);
        dt.backward(1000);
        sleep(300);
        dt.angle90(false);
        sleep(300);
        dt.forward(3000);
        sleep(300);
        */
    }
}
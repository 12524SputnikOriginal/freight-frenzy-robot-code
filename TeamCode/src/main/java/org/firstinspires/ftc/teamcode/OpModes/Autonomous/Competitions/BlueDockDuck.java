package org.firstinspires.ftc.teamcode.OpModes.Autonomous.Competitions;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import org.firstinspires.ftc.teamcode.Modules.Carousel;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;

@Autonomous(name = "BlueDockDuck")
@Disabled
public class BlueDockDuck extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Carousel cl = new Carousel(this);

        waitForStart();

        dt.forward(50, 0.3);

        cl.motion(.5);
        sleep(2000);
        cl.motion(0);

        dt.backward(3000, 0.5);

        dt.leftward(1500, 0.5);

        dt.stop();
    }

}

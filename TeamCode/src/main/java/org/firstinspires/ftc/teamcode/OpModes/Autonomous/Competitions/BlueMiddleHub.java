package org.firstinspires.ftc.teamcode.OpModes.Autonomous.Competitions;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
import org.firstinspires.ftc.teamcode.Modules.Intake.Intake;

@Autonomous(name = "BlueMiddleHub")
public class BlueMiddleHub extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Delivery dl = new Delivery(this);

        waitForStart();
        dt.forward(1500, 0.5);

        dl.openMotor();
        dl.closePush();
        sleep(3000);
        dl.openPush();
        dl.closeMotor();

        dt.backward(500, 0.5);

        dt.rotate(-90);

        dt.leftward(1500, 0.5);

        dt.forward(2500, 0.5);

        dt.forward(1500, 0.5);

        dt.rightward(2000, 0.5);

        dt.forward(1000, 0.5);

        dt.rotate(90);

        dt.leftward(1000, 0.5);

        dt.backward(500, 0.5);

        dt.stop();
    }
}

package org.firstinspires.ftc.teamcode.Modules;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;

import org.firstinspires.ftc.teamcode.OpModes.TeleOp.TeleOpMoscow;

/**
 * Класс датчика цвета
 */
public final class ColorSense {
    private final ColorSensor colorSensor;

    private static final float  NON_BLACK_POINT  = 30; // нижний порог яркости(V) поля
    private static final float  RED_H_POINT1     = 0; // нижний порог первого интервала цветового тона(H) красного скотча
    private static final int    RED_H_POINT2     = 20; // высший порог первого интервала цветового тона(H) красного скотча
    private static final int    RED_H_POINT3     = 310; // нижний порог второго интервала цветового тона(H) красного скотча
    private static final int    RED_H_POINT4     = 360; // высший порог второго интервала цветового тона(H) красного скотча
    private static final int    BLUE_H_POINT1    = 160; // нижний порог второго интервала цветового тона(H) синего скотча
    private static final int    BLUE_H_POINT2    = 270; // высший порог второго интервала цветового тона(H) синего скотча

    /**
     * Инициализация класса датчика
     * @param _opMode ссылка на opMode вызвавший конструктор
     */
    public ColorSense(LinearOpMode _opMode) {
        colorSensor = _opMode.hardwareMap.colorSensor.get("sensor_color");
    }

    /**
     * Метод получения HSV из RGB
     * @return массив значений HSV: H, S, V
     */
    public float[] getHsv() {
        float[] hsvValues = new float[3];
        Color.RGBToHSV(colorSensor.red() * 8, colorSensor.green() * 8, colorSensor.blue() * 8, hsvValues);
        return hsvValues;
    }

    /**
     * Метод определяет находится ли робот на линии или на поле
     * @return Находится ли робот на линии или нет
     */
    public boolean isOnLine() {
        return getHsv()[2] > NON_BLACK_POINT;
    }

    /**
     * Метод определяет находится ли робот на красной линии
     * @return Находится ли робот на красной линии или нет
     */
    public boolean isRedLine() {
        float[] hsv = getHsv();

        return ((hsv[0] > RED_H_POINT1) && (hsv[0] < RED_H_POINT2))
                || (hsv[0] > RED_H_POINT3) && (hsv[0] < RED_H_POINT4);
    }

    /**
     * Метод определяет находится ли робот на синей линии
     * @return Находится ли робот на синей линии
     */
    public boolean isBlueLine() {
        float[] hsv = getHsv();

        return (hsv[0] > BLUE_H_POINT1) && (hsv[0] < BLUE_H_POINT2);
    }
}

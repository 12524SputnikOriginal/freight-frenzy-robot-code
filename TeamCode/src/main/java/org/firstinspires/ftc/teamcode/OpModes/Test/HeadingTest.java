package org.firstinspires.ftc.teamcode.OpModes.Test;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import org.firstinspires.ftc.teamcode.Modules.ImuSensor;

@Autonomous(name = "ImuTest")
public class HeadingTest extends LinearOpMode{
    public ImuSensor imuSensor;
    public void runOpMode() {
        waitForStart();
        imuSensor = new ImuSensor(this);
        while (opModeIsActive()) {
            telemetry.addData("Heading:", imuSensor.getDegrees());
            telemetry.update();
        }
    }
}




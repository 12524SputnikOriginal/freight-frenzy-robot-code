package org.firstinspires.ftc.teamcode.Modules.Intake;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

public class Intake {
    private DcMotor motor;
    private boolean state;

    public Intake(LinearOpMode opMode) {
        motor = opMode.hardwareMap.dcMotor.get("intake");
    }

    /**
     * Устанавливает мощность 1 на мотор, если state == true, 0 если state == false
     */
    public void motion() {
        if (state) {
            motor.setPower(1);
            state = false;
        } else {
            motor.setPower(0);
            state = true;
        }
    }
    public void forward() {
        motor.setPower(1);
    }

    public void backward() {
        motor.setPower(-1);
    }

    public void stop() {
        motor.setPower(0);
    }
}


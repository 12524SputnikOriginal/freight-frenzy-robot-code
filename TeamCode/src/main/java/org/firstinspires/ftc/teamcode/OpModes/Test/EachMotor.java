package org.firstinspires.ftc.teamcode.OpModes.Test;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.robotcore.external.Telemetry;

@Autonomous(name = "EachMotor")
public class EachMotor extends LinearOpMode {

    @Override
    public void runOpMode() {
       DcMotor leftFrontDrive;
       DcMotor rightFrontDrive;
       DcMotor leftBackDrive;
       DcMotor rightBackDrive;
       LinearOpMode opMode;
       opMode = this;
       leftFrontDrive = opMode.hardwareMap.dcMotor.get("lf");
       rightFrontDrive = opMode.hardwareMap.dcMotor.get("rf");
       leftBackDrive = opMode.hardwareMap.dcMotor.get("lb");
       rightBackDrive = opMode.hardwareMap.dcMotor.get("rb");

       waitForStart();
       while (opModeIsActive()) {
           telemetry.addData("LeftFrontDrive:", leftFrontDrive.getPortNumber());
           telemetry.addData("RightFrontDrive:", rightFrontDrive.getPortNumber());
           telemetry.addData("LeftBackDrive:", leftBackDrive.getPortNumber());
           telemetry.addData("RightBackDrive:", rightBackDrive.getPortNumber());
           telemetry.update();

           telemetry.addLine("Process: LF - LeftFront");
           telemetry.update();
           /*
           leftFrontDrive.setPower(0.6);
           sleep(20000);
           leftFrontDrive.setPower(0);
           //leftFront;

           sleep(2000);

           telemetry.addLine("Process: RF - RightFront");
           telemetry.update();
           rightFrontDrive.setPower(0.6);
           sleep(20000);
           rightFrontDrive.setPower(0);

           sleep(2000);

           telemetry.addLine("Process: LB - LeftBack");
           telemetry.update();
           leftBackDrive.setPower(0.6);
           sleep(20000);
           leftBackDrive.setPower(0);

           sleep(2000);
            */
           telemetry.addLine("Process: RB - RightBack");
           telemetry.update();
           rightBackDrive.setPower(0.6);
           sleep(2000);
           rightBackDrive.setPower(0);
       }
   }
}

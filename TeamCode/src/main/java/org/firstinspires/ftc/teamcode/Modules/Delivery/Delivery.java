package org.firstinspires.ftc.teamcode.Modules.Delivery;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Delivery {
    private LinearOpMode opMode;
    private Servo motor;
    private Servo push;
    private boolean motorState = true;
    private boolean pushState = true;
    private Automate freightInject = new Automate();
    private ExecutorService executor;
    private boolean isFreightInjectInvoked = false;

    public Delivery (LinearOpMode _opMode) {
        opMode = _opMode;

        motor = opMode.hardwareMap.servo.get("delivery");
        push  = opMode.hardwareMap.servo.get("push");

        executor = Executors.newSingleThreadExecutor();
    }

    /**
     * Устанавливает позицию сервомотора на 0.7, если motorState, иначе на 0.2
     */
    public void motion() {
        if (motorState) {
            motor.setPosition(.7);
            motorState = false;
        } else {
            motor.setPosition(.2);
            motorState = true;
        }
    }

    /**
     * Устанавливает позицию сервомотора на 0.7, если pushState, иначе на 0.2
     */
    public void push() {
        if (pushState) {
            push.setPosition(.7);
            pushState = false;
        } else {
            push.setPosition(.1);
            pushState = true;
        }
    }

    public void freightInject() {
        if (!isFreightInjectInvoked){
            executor.execute(freightInject);
        }
    }

    public void freightInject(boolean state) {
        if (!state) {
            freightInject();
        } else {
            isFreightInjectInvoked = true;
            openMotor();
            closePush();
            opMode.sleep(1000);
            openPush();
            closeMotor();
            isFreightInjectInvoked = false;
        }
    }

    public void openMotor() {
        motor.setPosition(.2);
    }

    public void closeMotor() {
        motor.setPosition(.7);
    }

    public void openPush() {
        push.setPosition(.1);
    }

    public void closePush() {
        push.setPosition(.7);
    }

    private class Automate implements Runnable {
        @Override
        public void run() {
            isFreightInjectInvoked = true;
            openMotor();
            closePush();
            opMode.sleep(1000);
            openPush();
            closeMotor();
            isFreightInjectInvoked = false;
        }
    }
}

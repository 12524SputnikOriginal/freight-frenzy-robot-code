package org.firstinspires.ftc.teamcode.OpModes.Autonomous.Competitions;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Carousel;
import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
import org.firstinspires.ftc.teamcode.Modules.Intake.Intake;

@Autonomous(name = "BlueMiddleHubDuck")
public class BlueMiddleHubDuck extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Delivery dl = new Delivery(this);
        Carousel cl = new Carousel(this);
        //ставить нужно как можно ближе к правой границе плитки, чтобы робот не застревал на стыке граничных блоков, меняя направление
        waitForStart();
        dt.forward(1500, 0.5);

        dl.openMotor();
        dl.closePush();
        sleep(2000);
        dl.openPush();
        dl.closeMotor();

        dt.backward(500, 0.5);

        dt.rotate(90);

        dt.forward(1000, 0.5);

        dt.rightward(1500, 0.5);

        dt.forward(2000, 0.5);

        dt.forward(350, 0.3);

        cl.motion(0.5);
        sleep(2000);
        cl.motion(0);

        dt.backward(3000, 0.5);

        dt.rightward(1000, 0.5);

        dt.backward(1000, 0.5);

        dt.rightward(500, 0.5);

        dt.backward(2000, 0.5);

        dt.leftward(1000, 0.5);

        dt.backward(1000, 0.5);

        dt.rotate(-90);
        dt.stop();
    }
}

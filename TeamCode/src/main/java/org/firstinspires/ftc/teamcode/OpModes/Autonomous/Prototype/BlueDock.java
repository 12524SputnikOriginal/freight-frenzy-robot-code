package org.firstinspires.ftc.teamcode.OpModes.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Carousel;
import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
import org.firstinspires.ftc.teamcode.Modules.Intake.Intake;

@Autonomous(name = "Blue Dock w Duck and Hub")
@Disabled
public class BlueDock extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Carousel cl = new Carousel(this);
        Delivery dl = new Delivery(this);

        waitForStart();

        dt.driveSteps(0, 0.5, 0,200);
        sleep(1000);

        // карусель
        cl.motion(.5);
        sleep(2000);
        cl.motion(0);

        dt.driveSteps(-1, 0, 0, 100);
        sleep(500);
        dt.driveSteps(0, -1, 0, 1200);
        sleep(500);
        dt.driveSteps(0, 0, -1, 750);
        sleep(1000);
        dt.driveSteps(0, 1, 0, 400);

        // delivery
        dl.closeMotor();
        dl.closePush();
        sleep(2000);
        dl.openPush();
        dl.openMotor();

        dt.driveSteps(0, -1, 0, 900);
        sleep(1000);
        dt.driveSteps(0, 0, -1, 1500);
        sleep(1000);
        dt.driveSteps(0, 1, 0, 1500);

    }
}
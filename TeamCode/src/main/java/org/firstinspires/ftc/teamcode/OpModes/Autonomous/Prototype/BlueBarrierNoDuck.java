package org.firstinspires.ftc.teamcode.OpModes.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;

@Autonomous(name = "BlueBarrierNoDuck")
@Disabled
public class BlueBarrierNoDuck extends LinearOpMode{
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Delivery dl = new Delivery(this);

        waitForStart();

        dt.driveSteps(0, 1, 0, 1350);
        sleep(1000);

        // delivery
        dl.closeMotor();
        dl.closePush();
        sleep(3000);
        dl.openPush();
        dl.openMotor();
        sleep(1000);

        dt.driveSteps(0, -1, 0, 250);
        sleep(1000);

        dt.driveSteps(0, 0, -1, 1600);
        sleep(1000);

        dt.driveSteps(0, 1, 0, 3500);

        dt.stop();
    }

}
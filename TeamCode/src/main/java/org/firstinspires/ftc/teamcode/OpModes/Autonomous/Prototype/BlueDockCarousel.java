package org.firstinspires.ftc.teamcode.OpModes.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Carousel;
import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
import org.firstinspires.ftc.teamcode.Modules.Intake.Intake;

@Autonomous(name = "BlueDockCarousel")
@Disabled
public class BlueDockCarousel extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Carousel cl = new Carousel(this);

        waitForStart();

        dt.driveSteps(0, -1, 0,1000);
        sleep(1000);

        dt.driveSteps(-1, 0, 0, 1100);
        dt.driveSteps(0, 0, -1, 300);
        dt.driveSteps(0, 1, 0, 770);

        // карусель
        cl.motion(.5);
        sleep(2000);
        cl.motion(0);

        dt.driveSteps(0, -1, 0, 800);

        dt.stop();
    }
}
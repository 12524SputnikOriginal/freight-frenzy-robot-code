package org.firstinspires.ftc.teamcode.OpModes.Test;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;

@Autonomous(name = "Encoder No Turn")
public class EncoderTTesn extends LinearOpMode {

    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);

        waitForStart();

        for (int i = 0; i < 4; i++) {
            dt.driveSteps(0, 1, 0, 1000);
            dt.driveSteps(1, 0, 0, 1000);
        }

        sleep(200);
        dt.stop();
    }
}

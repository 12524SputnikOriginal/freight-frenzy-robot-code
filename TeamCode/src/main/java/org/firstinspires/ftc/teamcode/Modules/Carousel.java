package org.firstinspires.ftc.teamcode.Modules;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

public class Carousel {
    private DcMotor motor;

    public Carousel(LinearOpMode opMode) {
        motor = opMode.hardwareMap.dcMotor.get("carousel");
    }

    /**
     * Передает на мотор мощность равную power
     * @param power Сила подаваемая на мотор
     */
    public void motion(double power){
        motor.setPower(power);
    }
}

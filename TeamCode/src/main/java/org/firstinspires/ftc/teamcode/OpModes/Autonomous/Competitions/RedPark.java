package org.firstinspires.ftc.teamcode.OpModes.Autonomous.Competitions;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;

@Autonomous(name = "RedPark")
public class RedPark extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);


        waitForStart();

        dt.motionTillLine(0, 0.5, 0);

        dt.forward(1500, 0.5);

        dt.leftward(2000, 0.5);

        dt.forward(1000, 0.5);

        dt.rotate(-90);

        dt.leftward(1000, 0.5);

        dt.backward(500, 0.5);

        dt.stop();
    }
}

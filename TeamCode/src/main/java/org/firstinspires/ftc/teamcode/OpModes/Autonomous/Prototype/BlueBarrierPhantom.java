package org.firstinspires.ftc.teamcode.OpModes.Autonomous.Prototype;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Carousel;
import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
import org.firstinspires.ftc.teamcode.Modules.Intake.Intake;

@Autonomous(name = "BlueBarri")
@Disabled
public class BlueBarrierPhantom extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Delivery dl = new Delivery(this);

        waitForStart();

        dt.driveSteps(0, 1, 0, 1350);
        sleep(8000);

        // delivery
        dl.closeMotor();
        dl.closePush();
        sleep(3000);
        dl.openPush();
        dl.openMotor();
        sleep(1000);

        dt.driveSteps(0, -1, 0, 250);
        sleep(1000);

        dt.driveSteps(0, 0, -1, 2000);
        sleep(1000);

        dt.driveSteps(1, 0, 0, 4000);

        dt.driveSteps(0, 1, 0, 1500);
        //dt.rightward(500, 0.5);
        dt.driveSteps(-1, 0, 0, 2000);

        //dt.driveSteps(1, 0, 0, 900);

        dt.stop();
    }

}

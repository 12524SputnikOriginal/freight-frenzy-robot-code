package org.firstinspires.ftc.teamcode.OpModes.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;

@Autonomous(name = "RedBarrierNoDuck")
@Disabled
public class RedBarrierNoDuck extends LinearOpMode{
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Delivery dl = new Delivery(this);

        waitForStart();

        dt.driveSteps(0, 1, 0, 1200);
        sleep(1000);

        // delivery
        // delivery
        dl.closeMotor();
        dl.closePush();
        sleep(2000);
        dl.openPush();
        dl.closeMotor();
        sleep(1000);

        dt.driveSteps(0, -1, 0, 250);
        sleep(1000);

        dt.driveSteps(0, 0, -1, 1200);
        sleep(1000);

        dt.driveSteps(0, -1, 0, 3750);

        dt.stop();
    }

}
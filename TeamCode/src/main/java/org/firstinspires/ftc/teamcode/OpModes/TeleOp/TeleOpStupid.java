package org.firstinspires.ftc.teamcode.OpModes.TeleOp;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.Modules.Carousel;
//import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
import org.firstinspires.ftc.teamcode.Modules.Intake.Intake;

@TeleOp(name = "TeleOp Stupid")
public class TeleOpStupid extends LinearOpMode {
    private boolean a_btn_state = false;
    private boolean b_btn_state = false;
    private boolean y_btn_state = false;
    private boolean x_btn_state = false;

    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Intake     it = new Intake(this);
        Carousel   cl = new Carousel(this);
        Delivery   dl = new Delivery(this);

        waitForStart();

        while (opModeIsActive()) {
            // Если A на геймпаде 1 зажато, то передает мотору мощность 1
            // Если B на геймпаде 1 зажато, то передает мотору мощность -1
            // Иначе останавливает
            if (gamepad1.a) {
                it.forward();
            } else if (gamepad1.b) {
                it.backward();
            } else {
                it.stop();
            }
            
            // На Y на геймпаде 2 передает серве ворот позицию 0.7
            // На X на геймпаде 2 передает серве ворот позицию 0.2
            if (gamepad2.y) {
                dl.openMotor();
            } else if (gamepad2.x) {
                dl.closeMotor();
            }

            // На A на геймпаде 2 передает серве толкателя позицию 0.1
            // На B на геймпаде 2 передает серве толкателя позицию 0.7
            if (gamepad2.a) {
                dl.openPush();
            } else if (gamepad2.b) {
                dl.closePush();
            }

            // Передает отклонение левого стика на втором геймпаде по оси X
            cl.motion(
                gamepad2.left_stick_x / 2
            );

            // Основная езда
            dt.drive(
                -gamepad1.left_stick_x,
                -gamepad1.left_stick_y,
                gamepad1.right_trigger - gamepad1.left_trigger
            );
        }
    }
}
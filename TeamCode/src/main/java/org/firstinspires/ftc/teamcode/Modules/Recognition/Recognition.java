package org.firstinspires.ftc.teamcode.Modules.Recognition;
        import org.firstinspires.ftc.robotcore.internal.android.dx.rop.cst.Zeroes;
        import org.opencv.core.Core;
        import org.opencv.core.Mat;
        import org.opencv.core.Point;
        import org.opencv.core.Rect;
        import org.opencv.core.Scalar;
        import org.opencv.imgproc.Imgproc;
        import org.openftc.easyopencv.OpenCvPipeline;


public class  Recognition extends OpenCvPipeline {
    public int a = 50, b = 70, c = 70, d = 10, e = 207, thresh=92, maxval=255, one=109;
    public RingPosition position = RingPosition.ZERO;
    /*
     * перечисление для определения количесва колец
     */
    public enum RingPosition {
        ZERO,
        ONE,
        FOUR;
    }
    /*
     * константы цветов
     */
    static final Scalar BLUE = new Scalar(0, 0, 255);
    static final Scalar GREEN = new Scalar(0, 255, 0);
    /*
     * основные главные аргументы для определения расположения  и размера регионов выборки
     */
    /*int a1;
    Point REGION1_TOPLEFT_ANCHOR_POINT = new Point(a, b);
    Point REGION2_TOPLEFT_ANCHOR_POINT = new Point(a, c);
    final Point REGION3_TOPLEFT_ANCHOR_POINT = new Point(d, e);
    final int REGION_WIDTH = 50;
    final int REGION_HEIGHT = 20; */
    /*
     * точки, которые определяют регион прямоугольников, полученные из вышеуказанных значений
     * Точка А- левый верхний угол прямоугольника, Точка В- правый нижний угол прямоугольник
     * присваиваем  им формулы по осям
     *   ----------------------------------
     *   |                                  |
     *   | (0,0) Point A                    |
     *   |                                  |
     *   |                                  |
     *   |                                  |
     *   |                                  |
     *   |                                  |
     *   |                  Point B (70,50) |
     *   ----------------------------------
     *
     */
    /*Point region1_pointA = new Point(
            REGION1_TOPLEFT_ANCHOR_POINT.x,
            REGION1_TOPLEFT_ANCHOR_POINT.y);
    Point region1_pointB = new Point(
            REGION1_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
            REGION1_TOPLEFT_ANCHOR_POINT.y + 20);
    Point region2_pointA = new Point(
            REGION2_TOPLEFT_ANCHOR_POINT.x,
            REGION2_TOPLEFT_ANCHOR_POINT.y);
    Point region2_pointB = new Point(
            REGION2_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
            REGION2_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);
    Point region3_pointA = new Point(
            REGION3_TOPLEFT_ANCHOR_POINT.x,
            REGION3_TOPLEFT_ANCHOR_POINT.y);
    Point region3_pointB = new Point(
            REGION3_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
            REGION3_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);*/
    /*
     * используемые переменные в Mat
     */
    Mat region1_Cb, region2_Cb, region3_Cb;
    Mat HSV = new Mat();
    Mat Cb = new Mat();
    Mat Bin = new Mat();
    int avg1, avg2, avg3;
    /*
     * Эта функция принимает кадр RGB, преобразовывает  в YCrCb,
     * и извлекает канал Cr в переменную 'Cr' или 'Сb'!!!!!!!!!!!!!!!!
     */
    void inputToCb(Mat input) {
        Imgproc.cvtColor(input, Cb, Imgproc.COLOR_RGB2YCrCb);
        Core.extractChannel(Cb, Bin, 2);
        Imgproc.threshold(Bin, Bin, thresh, maxval, Imgproc.THRESH_BINARY_INV);
    }
    @Override
    public void init(Mat firstFrame) {
        /*
         *Нам нужно вызвать это, чтобы убедиться, что "Cb"
         * объект инициализируется, так что подматы, которые мы делаем
         * будут по-прежнему привязаны к нему в последующих кадрах. (Если
         * объект нужно было инициализировать только в processFrame,
         * тогда субматарицы будут отсоединены, потому что
         * буфер будет перераспределен при первом реальном кадре)
         */
        inputToCb(firstFrame);
        /*
         * ПодМаты  - это постоянная ссылка на область родительского (главного)
         * буфера. Любые изменения в младшем элементе влияют на родителя и наоборот
         */
        /*region1_Cb = Bin.submat(new Rect(region1_pointA, region1_pointB));
        region2_Cb = Bin.submat(new Rect(region2_pointA, region2_pointB));
        region3_Cb = Bin.submat(new Rect(region3_pointA, region3_pointB));*/
    }
    @Override
    public Mat processFrame(Mat input) {
            /*
            * Сначала мы преобразуем в цветовое пространство YCrCb из цветового пространства RGB.
            * так как в цветовом пространстве RGB, цветности и
            * яркости переплетаются. В YCrCb цветность и яркость разделены.
            * YCrCb - это 3-канальное цветовое пространство, как и RGB. 3 канала YCrCb:
            * Y, канал яркости (который, по сути, просто черно-белое изображение),
            * Канал Cr, который записывает отличие от красного, и канал Cb,
            * который регистрирует отличие от синего. Потому что цветность и яркость
            * не относится к YCrCb, код видения, написанный для поиска определенных значений
            * в каналах Cr / Cb не сильно пострадают от различных
            * интенсивностей света, поскольку эта разница, скорее всего, будет
            * отражено в канале Y.
            * После преобразования в YCrCb мы извлекаем только второй канал,
            * Канал Cb. Мы делаем это потому, что кольца ярко-желтые и контрастные.
            * Затем мы берем среднее значение пикселя в 2 разных регионах на этом Cb
            * канал. эти  2 региона означает, что там точно есть кольцо
            *потом мы сравниваем: если 3 меньше 2 среднего значения региона, то 4 кольца. а когда 3 среднее значение региона меньше 1 региона, то одно кольцо.
            если ничего не закрашено, то 0 колец.
            * Мы также рисуем прямоугольники на экране, показывая, где находятся образцы регионов.
            * Чтобы весь этот процесс работал правильно, каждая область образца
            * должна быть расположена в центре каждого из первого кольца
            */
        /*
         *Получить канал Cb входного кадра после преобразования в YCrCb
         */
        inputToCb(input);
        /*
         * Вычислите среднее значение пикселя для каждой области ПодМата.
         * берем  среднее значение для одноканального буфера, поэтому значение
         * нам нужен индекс 0. Мы также могли взять среднее
         * значение в пикселях 3-канального изображения и указанное значение
         * здесь по индексу 2
         */
        int a1;
        Point REGION1_TOPLEFT_ANCHOR_POINT = new Point(a, b);
        Point REGION2_TOPLEFT_ANCHOR_POINT = new Point(one, c);
        final Point REGION3_TOPLEFT_ANCHOR_POINT = new Point(d, e);
        final int REGION_WIDTH = 50;
        final int REGION_HEIGHT = 20;

        Point region1_pointA = new Point(
                REGION1_TOPLEFT_ANCHOR_POINT.x,
                REGION1_TOPLEFT_ANCHOR_POINT.y);
        Point region1_pointB = new Point(
                REGION1_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
                REGION1_TOPLEFT_ANCHOR_POINT.y + 20);
        Point region2_pointA = new Point(
                REGION2_TOPLEFT_ANCHOR_POINT.x,
                REGION2_TOPLEFT_ANCHOR_POINT.y);
        Point region2_pointB = new Point(
                REGION2_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
                REGION2_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);
        Point region3_pointA = new Point(
                REGION3_TOPLEFT_ANCHOR_POINT.x,
                REGION3_TOPLEFT_ANCHOR_POINT.y);
        Point region3_pointB = new Point(
                REGION3_TOPLEFT_ANCHOR_POINT.x + REGION_WIDTH,
                REGION3_TOPLEFT_ANCHOR_POINT.y + REGION_HEIGHT);

        region1_Cb = Bin.submat(new Rect(region1_pointA, region1_pointB));
        region2_Cb = Bin.submat(new Rect(region2_pointA, region2_pointB));
        region3_Cb = Bin.submat(new Rect(region3_pointA, region3_pointB));

        avg1 = (int) Core.mean(region1_Cb).val[0];
        avg2 = (int) Core.mean(region2_Cb).val[0];
        avg3 = (int) Core.mean(region3_Cb).val[0];
        /*
         * Нарисуйте прямоугольник, показывающий на экране область 1 образца.
         * */
        Imgproc.rectangle(
                input, // буфер для рисования
                region1_pointA, // первая точка, которая распознает прямоугольник
                region1_pointB, // вторая точка, которая распознает прямоугольник
                BLUE, //  Цвет, которым нарисован прямоугольник
                5); // Толщина линий прямоугольника

        /*
         * Нарисуйте прямоугольник, показывающий на экране область 2 образца.
         * */
        Imgproc.rectangle(
                input, // буфер для рисования
                region2_pointA, // первая точка, которая распознает прямоугольник
                region2_pointB, // вторая точка, которая распознает прямоугольник
                BLUE, // Цвет, которым нарисован прямоугольник
                2); // Толщина линий прямоугольника

        /*
         *Нарисуйте прямоугольник, показывающий на экране область 3 образца.

         */
        Imgproc.rectangle(
                input, // буфер для рисования
                region3_pointA, // первая точка, которая распознает прямоугольник
                region3_pointB, // вторая точка, которая распознает прямоугольник
                BLUE, // Цвет, которым нарисован прямоугольник
                2); // Толщина линий прямоугольника


        /*
         * находим максмум из 2 средних
         */
        int max = Math.max(avg1, avg2);

        /*
         *Теперь, когда мы нашли максимум, нам нужно пойти и
         * выяснить, из какой области образца это значение было
         */
        if ((avg3 < avg2) && (Math.abs(avg1 - avg2) < 210))  {
            position = RingPosition.FOUR; //

            /*

             * Нарисуйте сплошной прямоугольник поверх выбранной области.

             */
            Imgproc.rectangle(
                    input, // буфер для рисования
                    region2_pointA, // первая точка, которая распознает прямоугольник
                    region2_pointB, //вторая точка, которая распознает прямоугольник
                    GREEN, // Цвет, которым нарисован прямоугольник
                    -1); // Отрицательная толщина означает сплошную заливку
        }

        else {
            if (avg3 < avg1) {
                position = RingPosition.ONE; //

                /*
                 * Нарисуйте сплошной прямоугольник поверх выбранной области.

                 */
                Imgproc.rectangle(
                        input, // буфер для рисования
                        region1_pointA, // первая точка, которая распознает прямоугольник
                        region1_pointB, // вторая точка, которая распознает прямоугольник
                        GREEN, // Цвет, которым нарисован прямоугольник
                        -1); // Отрицательная толщина означает сплошную заливку

            }
            else {
                position = RingPosition.ZERO;
            }
        }



        /*

         *Визуализируйте буфер 'input' в область просмотра. Но обратите внимание, это не
         * просто рендеринг видео с камеры, потому что мы вызвали функции
         * чтобы добавить аннотации к этому буферу до этого (раньше).
         */
        return input;
    }

    /*
     * вызываем это из OpMode поток, чтобы получить последний анализ
     */
    public RingPosition getAnalysis() {
        return position;
    }

    public int[] getAvgs(){
        int[] res= {avg1,avg2,avg3};
        return res;
    }
}






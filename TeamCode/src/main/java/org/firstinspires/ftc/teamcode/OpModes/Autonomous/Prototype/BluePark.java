package org.firstinspires.ftc.teamcode.OpModes.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.checkerframework.checker.units.qual.C;
import org.firstinspires.ftc.teamcode.Modules.Carousel;
import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;

@Autonomous(name = "OldBluePark")
@Disabled
public class BluePark extends LinearOpMode{
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Carousel cl = new Carousel(this);

        waitForStart();

    

        dt.stop();
    }
}
package org.firstinspires.ftc.teamcode.OpModes.Autonomous.Competitions;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Carousel;
import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
import org.firstinspires.ftc.teamcode.Modules.Intake.Intake;

@Autonomous(name = "RedMiddleHubDuck")
public class RedMiddleHubDuck extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Intake it = new Intake(this);
        Delivery dl = new Delivery(this);
        Carousel cl = new Carousel(this);

        waitForStart();
        dt.forward(1500, 0.5);

        dl.openMotor();
        dl.closePush();
        sleep(2000);
        dl.openPush();
        dl.closeMotor();

       /* dt.backward(500, 0.5);

        dt.rotate(-90);

        dt.leftward(1500, 0.5);

        dt.forward(2250, 0.5);

        dt.rightward(1500, 0.5);

        dt.rotate(-45);

        dt.forward(500, 0.5);

        cl.motion(0.5);
        sleep(2000);
        cl.motion(0);

        dt.backward(1000, 0.5);

        dt.leftward(500, 0.5);

        dt.backward(750, 0.5);

        dt.rightward(1000, 0.5);

        dt.backward(500, 0.5);

        dt.rotate(90);

        dt.stop();*/
        dt.backward(200, 0.5);
        dt.rotate(-90);
        dt.forward(2750, 0.5);
        dt.rotate(-45);
        dt.forward(750, 0.5);

        cl.motion(0.5);
        sleep(2000);
        cl.motion(0);

        dt.leftward(400, 0.5);
        dt.rotate(-90);
        dt.rightward(500, 0.5);
        dt.forward(2000, 0.5);

    }
}

package org.firstinspires.ftc.teamcode.Modules.Drivetrain;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.Modules.ColorSense;
import org.firstinspires.ftc.teamcode.Modules.ImuSensor;

public class Drivetrain {
    private DcMotor leftFrontDrive;
    private DcMotor rightFrontDrive;
    private DcMotor leftBackDrive;
    private DcMotor rightBackDrive;
    private LinearOpMode opMode;
    private ColorSense colorSense;
    private ImuSensor imu;
    private final double GYRO_COURSE_PRECISION = 1;
    /**
     * Инициализирует моторы робота
     * lf - левый передний
     * rf - правый передний
     * lb - левый задний
     * rb - правый задний
     * @param _opMode ссылка на opMode вызвавший конструктор
     */
    public Drivetrain(LinearOpMode _opMode) {
        opMode = _opMode;

        leftFrontDrive = opMode.hardwareMap.dcMotor.get("lf");
        rightFrontDrive = opMode.hardwareMap.dcMotor.get("rf");
        leftBackDrive = opMode.hardwareMap.dcMotor.get("lb");
        rightBackDrive = opMode.hardwareMap.dcMotor.get("rb");
        colorSense = new ColorSense(opMode);
        imu = new ImuSensor(opMode);

    }

    /**
     * Устанавливает силу на моторы соответсвенно формулам
     * @param x Координата по оси X
     * @param y Координата по оси Y
     * @param r Координата для поворота
     */
    public void drive(double x, double y, double r) {
        leftFrontDrive.setPower(-x - y - r);
        rightFrontDrive.setPower(-x + y - r);
        leftBackDrive.setPower(x - y - r);
        rightBackDrive.setPower(x + y - r);
    }

    /**
     * Устанавливает силу на моторы из массива с 4 элементами
     * @param powers Массив с 4 элементами соотвественно моторам
     */
    public void drive(double powers[]) {
        leftFrontDrive.setPower(powers[0]);
        rightFrontDrive.setPower(powers[1]);
        leftBackDrive.setPower(powers[2]);
        rightBackDrive.setPower(powers[3]);
    }
    /**
     * Устанавливает режим mode на каждый мотор соотвественно
     * @param mode Режим для установки
     */
    public void setMode(DcMotor.RunMode mode) {
        leftFrontDrive.setMode(mode);
        rightFrontDrive.setMode(mode);
        leftBackDrive.setMode(mode);
        rightBackDrive.setMode(mode);
    }

    /**
     * Проверяет заняты ли все моторы или проверяет подается ли на моторы достаточная сила
     * @return Правдиво если моторы заняты и подается достаточная сила
     */
    public boolean isBusy() {
        boolean busy = leftFrontDrive.isBusy() &&
                rightFrontDrive.isBusy() &&
                leftBackDrive.isBusy() &&
                rightBackDrive.isBusy();

        boolean pows = (Math.abs(leftFrontDrive.getPower()) +
                Math.abs(rightFrontDrive.getPower()) +
                Math.abs(leftBackDrive.getPower()) +
                Math.abs(rightBackDrive.getPower())) > 0.05;

        return busy && pows;
    }

    /**
     * Рассчитывает позицию для энкодеров и силу для мотора
     * @param x Скорость по X
     * @param y Скорость по Y
     * @param r Скорость поворота
     * @param tick Время тиков
     */
    public void driveSteps(double x, double y, double r, int tick) {
        setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        double[] powers = calculatePower(x, y, r);

        leftFrontDrive.setTargetPosition(tick * sign(powers[0]));
        rightFrontDrive.setTargetPosition(tick * sign(powers[1]));
        leftBackDrive.setTargetPosition(tick * sign(powers[2]));
        rightBackDrive.setTargetPosition(tick * sign(powers[3]));

        setMode(DcMotor.RunMode.RUN_TO_POSITION);
        drive(powers);

        while (isBusy() && opMode.opModeIsActive()) {}

        stop();
        setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

    /**
     * Устанавливает нулевую силу на моторы, останавливая их
     */
    public void stop() {
        drive(0, 0, 0);
    }

    /**
     * Двигается вперед t тиков и останавливается
     * @param t количество тиков
     */
    public void forward(int t, double y ) {
        driveSteps(0, y, 0, t);
        stop();
    }

    /**
     * Двигается назад t тиков и останавливается
     * @param t количество тиков
     */
    public void backward(int t, double y ) {
        driveSteps(0, -y, 0, t);        
        stop();
    }

    /**
     * Двигается влево t тиков и останавливается
     * @param t количество тиков
     */
    public void leftward(int t, double x ) {
        driveSteps(x, 0, 0, t);
        stop();
    }

    /**
     * Двигается вправо t тиков и останавливается
     * @param t количество тиков
     */
    public void rightward(int t, double x) {
        driveSteps(-x, 0, 0, t);
        stop();
    }

    /**
     * Осуществляет поворот на 90 градусов
     * @param isRight В какую сторону повернуть
     */
    public void angle90(boolean isRight) {
        drive(0, 0, isRight ? 1 : -1);
        opMode.sleep(900);
        stop();
    }

    /**
     * Осуществляет поворот на 120 градусов
     * @param isRight В какую сторону повернуть
     */
    public void angle120 (boolean isRight) {
        drive(0, 0, isRight ? 1 : -1);
        opMode.sleep(800);
        stop();
    }

    /**
     * Осуществляет поворот на 5 градусов
     * @param isRight В какую сторону повернуть
     */
    public void angle5(boolean isRight) {
        drive(0, 0, isRight ? 1 : -1);
        opMode.sleep(350);
        stop();
    }

    /**
     * Определяет знак числа. положительное - 1, отрицательное - -1, нуль - 0.
     * Обертка вокруг
     * @param num Число для определения знака
     * @return Число отражающее знак данного числа
     */
    public int sign(double num) {
        //num >> 31 | -num >> 31
        //num > 0 ? 1: num < 0 ? -1: 0;
        return (int)Math.signum(num);
    }

    /**
     * Движение до обнаружения линии/отличной от поля плоскости
     * не рекомендуется ставить большую мощность  -> датчик не замечает линию
     * @param x - мощность на моторы лево/право
     * @param y - мощность на моторы вперёд/назад
     * @param r - мощность на моторы поворот
     */
    public void motionTillLine(double x, double y, double r) {
        while (!colorSense.isOnLine() && opMode.opModeIsActive()) {
            drive(x, y, r);
        }
        stop();
    }
    
    /**
    * Поворот робота на курс(с) от его положения инициализации
    * @param c - курс в градусах
    */
    public void setCourse(double c) {
    	while ((imu.getDegrees() < c - GYRO_COURSE_PRECISION)||(imu.getDegrees() >  c + GYRO_COURSE_PRECISION)) {
    		drive(0, 0, 0.5);
            opMode.telemetry.addData("Angle:", imu.getDegrees());
            opMode.telemetry.update();
    	}
    	stop();
    }
    /**
    * Поворот робота на градус(d) от его положения в момент
    * @param d - градус
    */
    public void rotate(double d) {
        d = -d;
        int dSign = -sign(d);
        d += imu.getDegrees();
        if (d < -180) {
            d += 360;
        }
        if (d > 180) {
            d -= 360;
        }
        while((imu.getDegrees() < d - GYRO_COURSE_PRECISION)||(imu.getDegrees() > d + GYRO_COURSE_PRECISION)) {
    	    drive(0, 0, 0.6 * dSign);
            opMode.telemetry.addData("Angle:", imu.getDegrees());
            opMode.telemetry.addData("D", d);
            opMode.telemetry.update();
        }
    	stop();
    }
    /**
     * Расчёт значений мощности на моторы
     * @param x мощность по X
     * @param y мощность по Y
     * @param r мощность поворота
     * @return массив значений мощности
     */
    public double[] calculatePower(double x, double y, double r) {
        return new double[] {
                -x - y - r, // left top
                -x + y - r, // right top
                x - y - r, // left bot
                x + y - r // right bot
        };
    }

}

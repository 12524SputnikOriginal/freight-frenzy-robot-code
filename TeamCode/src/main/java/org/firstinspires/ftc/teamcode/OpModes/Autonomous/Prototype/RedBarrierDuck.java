package org.firstinspires.ftc.teamcode.OpModes.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.Modules.Carousel;
import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
import org.firstinspires.ftc.teamcode.Modules.Intake.Intake;

@Autonomous(name = "RedBarrierDuck")
@Disabled
public class RedBarrierDuck extends LinearOpMode {
    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Carousel cl = new Carousel(this);
        Intake it = new Intake(this);
        Delivery dl = new Delivery(this);

        waitForStart();

        dt.driveSteps(0, 1, 0, 1200);
        sleep(1000);

        // delivery
        dl.closeMotor();
        dl.closePush();
        sleep(2000);
        dl.openPush();
        dl.openMotor();

        dt.driveSteps(0, -1, 0, 220);
        sleep(1000);

        dt.driveSteps(0, 0, 1, 2500);
        sleep(1000);

        dt.driveSteps(-1, 0, 0, 1400);
        sleep(1000);

        dt.driveSteps(0, 1, 0, 2800);
        sleep(1000);

        cl.motion(-0.5);
        sleep(2000);
        cl.motion(0);

        dt.driveSteps(0, -1, 0, 4700);

        dt.stop();
    }
}
package org.firstinspires.ftc.teamcode.OpModes.TeleOp;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.Modules.Carousel;
import org.firstinspires.ftc.teamcode.Modules.Delivery.Delivery;
import org.firstinspires.ftc.teamcode.Modules.Drivetrain.Drivetrain;
import org.firstinspires.ftc.teamcode.Modules.Intake.Intake;

@TeleOp(name = "TeleOp Competitions")
public class TeleOpCompetitions extends LinearOpMode {
    private boolean aBtnState = false;
    private boolean bBtnState = false;
    private boolean yBtnState = false;
    private boolean xBtnState = false;
    private boolean lbBtnState = false;

    @Override
    public void runOpMode() {
        Drivetrain dt = new Drivetrain(this);
        Intake it = new Intake(this);
        Carousel cl = new Carousel(this);
        Delivery dl = new Delivery(this);

        waitForStart();

        while (opModeIsActive()) {
            telemetry.addData("aBtnState = ", aBtnState);
            telemetry.addData("bBtnState = ", bBtnState);
            telemetry.addData("yBtnState = ", yBtnState);
            telemetry.update();

            // Первое нажатие включает захват с мощьностью 1
            // Второе нажатие выключает захват
            if (gamepad2.a && !aBtnState) {
                it.motion();
            }

            aBtnState = gamepad2.a;

            // Первое нажатие открывает выбрасыватель
            // Второе нажатие закрывает выбрасыватель
            if (gamepad2.b && !bBtnState) {
                dl.motion();
            }

            bBtnState = gamepad2.b;

            // Первое нажатие открывает толкатель
            // Второе нажатие закрывает толкатель
            if (gamepad2.y && !yBtnState) {
                dl.push();
            }
            yBtnState = gamepad2.y;
            if (gamepad2.x && !xBtnState){
                it.reverseMotion();
            }
            xBtnState = gamepad2.x;
            if (gamepad2.left_bumper && !lbBtnState){
                dl.freightInject();
            }
            lbBtnState = gamepad2.left_bumper;
            // Передает отклонение левого стика на втором геймпаде по оси X
            cl.motion(
                    gamepad2.left_stick_x * 0.6
            );

            // Основная езда
            dt.drive(
                    -gamepad1.left_stick_x,
                    -gamepad1.left_stick_y,
                    gamepad1.right_trigger - gamepad1.left_trigger
            );

            sleep(5);
        }
    }
}
